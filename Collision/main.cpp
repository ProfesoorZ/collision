#include <iostream>
#include <string>
#include <thread>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

#define WIDTH 1920
#define HEIGHT 1080
#define MEU 0.005
#define G 10

using namespace sf;

class Ball
{
private:
	float radius;
	float mass;
	bool hasFriction;
	CircleShape* circle;
	Vector2f velocity;
public:
	//constructor - initiates the ball with the setting given
	Ball(Color color, float radius, Vector2f startPos, Vector2f initialSpeed, float mass, bool hasFriction) 
	{ 
		circle = new CircleShape(radius); 
		circle->setFillColor(color); 
		this->radius = radius; 
		velocity = initialSpeed; 
		circle->setPosition(startPos); 
		this->mass =  mass;
		this->hasFriction = hasFriction;
	}
	//destrcutor
	~Ball() {};
	//checks for collision with the frame of the engine
	void checkBoundsCollisions() {
		//top
		if (circle->getPosition().y < 0) {
			circle->setPosition(circle->getPosition().x, 0.f);
			velocity.y = -velocity.y;
		}
		//left
		if (circle->getPosition().x < 0) {
			circle->setPosition(0.f, circle->getPosition().y);
			velocity.x = -velocity.x;
		}
		//right
		if (circle->getPosition().x + circle->getGlobalBounds().width > WIDTH) {
			circle->setPosition(WIDTH - circle->getGlobalBounds().width, circle->getPosition().y);
			velocity.x = -velocity.x;
		}
		//bot
		if (circle->getPosition().y + circle->getGlobalBounds().height > HEIGHT) {
			circle->setPosition(circle->getPosition().x, HEIGHT - circle->getGlobalBounds().height);
			velocity.y = -velocity.y;
		}
	};
	//checks if two balls are coliding
	bool isColiding(Ball* other){
		//if the distance between the centers of the balls equal to the sum of their radiuses
		return std::sqrt(std::pow((circle->getPosition().x + radius) - (other->getCircle()->getPosition().x + other->getRadius()), 2) + std::pow(circle->getPosition().y - other->getCircle()->getPosition().y, 2)) <= ((float)radius + (double)other->getCircle()->getRadius());
	}
	//calculate the velocities of each ball after the collision
	void collision(Ball* other){
		float u1x, u1y, u2x, u2y;
		//combination between the two formulas-conservation of momentum and conservation of kinetic energy
		u1x = ((mass - other->getMass()) * velocity.x) / (mass + other->getMass()) + ((2 * other->getMass() * other->getVelocity().x) / (mass + other->getMass()));
		u2x = ((2 * mass * velocity.x) / (mass + other->getMass())) + ((other->getMass() - mass) * other->getVelocity().x) / (mass + other->getMass());
		u1y = ((mass - other->getMass()) * velocity.y) / (mass + other->getMass()) + ((2 * other->getMass() * other->getVelocity().y) / (mass + other->getMass()));
		u2y = ((2 * mass * velocity.y) / (mass + other->getMass())) + ((other->getMass() - mass) * other->getVelocity().y) / (mass + other->getMass());

		this->setVelocity(Vector2f(u1x, u1y));
		other->setVelocity(Vector2f(u2x, u2y));
	}
	//chnge the velocity if has friction enabled
	void changeVelocityWithFriction(Ball* ball)
	{
		while (hasFriction)
		{
			sleep(Time(seconds(0.1)));
			float accelaration = G * MEU;
			float newX = ball->getVelocity().x > 0 ? ball->getVelocity().x - accelaration : (ball->getVelocity().x < 0 ? ball->getVelocity().x + accelaration : 0), newY = ball->getVelocity().y > 0 ? ball->getVelocity().y - accelaration : (ball->getVelocity().y < 0 ? ball->getVelocity().y + accelaration: 0);
			ball->setVelocity(Vector2f(newX, newY));
		}
	}
	//return the circle
	CircleShape* getCircle() { return circle; }
	//returns the velocity of the ball
	Vector2f getVelocity() { return velocity; }
	//returns the radius of the circle
	float getRadius() { return radius; }
	//returns the mass of the ball
	float getMass() { return mass; }
	//set a new velocity to the ball
	void setVelocity(Vector2f newVelocity) { velocity = newVelocity; }
};

int main()
{
	RenderWindow window(VideoMode(WIDTH, HEIGHT), "Collision");
	Event event;
	Clock dt_clock;
	float dt, dx, dy;

	//creating three balls
	Ball* ball1 = new Ball(Color::Green, 100.f, Vector2f(775, 300), Vector2f(1, 1), 10, true);
	Ball* ball2 = new Ball(Color::Blue, 100.f, Vector2f(570, 300), Vector2f(1, 3), 10, false);
	/*Ball* ball3 = new Ball(Color::Red, 100.f, Vector2f(300, 600), Vector2f(4, 0), 10, false);*/

	/*std::thread friction1(&Ball::changeVelocityWithFriction, ball1, std::ref(ball1));
	friction1.detach();*/

	while (window.isOpen())
	{
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
		}

		//checks for collisions of each ball with the frame
		ball1->checkBoundsCollisions();
		ball2->checkBoundsCollisions();
		/*ball3->checkBoundsCollisions();*/

		//ball1->changeVelocityWithFriction(ball1);

		//cehcks if two balls are coliding
		if (ball1->isColiding(ball2))
			ball1->collision(ball2);
		/*if (ball2->isColiding(ball3))
			ball2->collision(ball3);
		if (ball1->isColiding(ball3))
			ball1->collision(ball3);*/

		std::cout << ball1->getVelocity().x << "," << ball1->getVelocity().y << std::endl;

		/*ball3->getCircle()->move(ball3->getVelocity());*/
		ball2->getCircle()->move(ball2->getVelocity());
		ball1->getCircle()->move(ball1->getVelocity());
		window.clear();
		window.draw(*(ball1->getCircle()));
		window.draw(*(ball2->getCircle()));
		/*window.draw(*(ball3->getCircle()));*/
		window.display();
	}
	delete ball1;
	//delete ball2;
	//delete ball3;
	system("pause");
	return 0;
}